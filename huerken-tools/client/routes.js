  angular.module('HuerkenTool').config(['$urlRouterProvider', '$stateProvider', '$locationProvider', 
    function($urlRouterProvider, $stateProvider, $locationProvider){
      $locationProvider.html5Mode(true);

      $stateProvider
        .state('channel', {
          url: '/channel/:channel_token/:user_token',
          templateUrl: 'client/messages/views/message-channel.ng.html',
          controller: 'MessageChannelCtrl',
        });
      //   .state('partyDetails', {
      //     url: '/party/:partyId',
      //     templateUrl: 'client/parties/views/party-details.ng.html',
      //     controller: 'PartyDetailsCtrl',
      //     resolve: {
      //       'currenUser': ['$meteor', function($meteor){
      //         return $meteor.requireUser();
      //       }]
      //     }
      //   });

        $urlRouterProvider.otherwise('/channel/asd123/asdf1234');
  }]);

  // angular.module('HuerkenTool').run(['$rootScope', '$state', 
  // 	function($rootScope, $state){
	 //    // $rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error){
	 //    //   if(error === 'AUTH_REQUIRED'){
	 //    //     $state.go('parties');
	 //    //   }
	 //    // });
  // }]);