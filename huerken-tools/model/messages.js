HtChannels = new Mongo.Collection('HtChannels', {});
HtChannelMessages = new Mongo.Collection('HtChannelMessages', {});
HtRandomNames = new Mongo.Collection('HtRandomNames', {});

// Crear una nueva coleccion por cada item que deceo este indexado (tenga un _id),
// tener en cuenta que indexar data, mejora la performance en grandes busquedas, vs el anidar data dentro del documento